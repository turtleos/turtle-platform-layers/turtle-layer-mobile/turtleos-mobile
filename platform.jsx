/*
  Mobile Platform Layer
*/

import React from 'react';

import { MobileWM } from './components/mobile-wm/wm';

import { MobileWindow } from './components/mobile-wm/mobile-window';

const PLATFORM = {
    expects: ( width, height ) => {
        //remove || true when adding more platforms
        if( width < height) return true;
        return false;
    },
    render: () => {
        //Init
        return (
            <MobileWM/>
        );
    },
    window: (config) => {
        return (
            <MobileWindow {...config}/>
        );
    }
};

export {
    PLATFORM
}
