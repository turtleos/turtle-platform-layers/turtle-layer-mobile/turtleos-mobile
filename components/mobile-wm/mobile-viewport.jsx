import React from 'react';

function MobileViewport({shown, windows}) {
    return (
        <div style={{
            height:'calc(100%)',
            top:(shown)?0:'101%',
            transition:'top .4s',
            left:0,
            width:'calc(100%)',
            background:'rgba(0,0,0,0.4)',
            position:'absolute',
        }}>
          {windows}
        </div>
    );
}

export {
    MobileViewport
}
