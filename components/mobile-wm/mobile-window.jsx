import React from 'react';

import * as Feather from 'react-feather';


import { getWindowDimensions } from '../../../../../lib/turtleos-platform-tools/platform-tools';

import { useSelector } from 'react-redux';


function MobileWindow(props) {
    const vim = useSelector(state=>state.vim);
    const am = useSelector(state=>state.am);
    const [focus, setFocus] = React.useState(vim.get());

    props.bridge.addEventListener('focus',()=>{
        let num = vim.get();
        num++;
        vim.set(num);
        setFocus(num);
    });

    return (
              <div className="window-viewport" style={{
                  width: '100%',
                  height: '100%',
                  background: '#504945',
                  position: 'absolute',
                  left:0,
                  top:0,
                  zIndex:focus
                  }}>
                {props.view}
            </div>
    );
}

export {
    MobileWindow
}
